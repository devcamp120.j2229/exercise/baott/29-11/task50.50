package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V000000";
    public String getVoucherCode(){
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode){
        this.voucherCode = voucherCode;
    }
    public void showVoucher(){
        System.out.println("Voucher code is: " + this.voucherCode);
    }
    public void showVoucher(String voucherCode){
        System.out.println("This is voucher code: " + this.voucherCode);
        System.out.println("This is voucher code too: "+ voucherCode);
    }
    public static void main(String[] args){
        Voucher voucher = new Voucher();
        // Comment dòng thực thi
        voucher.showVoucher();
        // Code COment dưới đây
        String code = "VOUCHER";
        voucher.setVoucherCode(code);
        System.out.println(voucher.getVoucherCode());
        voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();
    }
}
